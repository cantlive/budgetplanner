﻿using Microsoft.EntityFrameworkCore;
using BudgetPlanner.Model;

namespace BudgetPlanner.Core
{
    public class SqlLiteDataBaseContext : DbContext
    {
        public DbSet<Transaction> Transactions { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=BudgetPlanner.db");
        }
    }
}
