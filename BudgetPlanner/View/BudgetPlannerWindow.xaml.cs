﻿using BudgetPlanner.ViewModel;
using System.Windows;

namespace BudgetPlanner.View
{
    /// <summary>
    /// Окно планировщика бюджета.
    /// </summary>
    public partial class BudgetPlannerWindow : Window
    {
        /// <summary>
        /// Создает экземпляр <see cref="BudgetPlannerWindow"/>
        /// </summary>
        public BudgetPlannerWindow()
        {
            InitializeComponent();
            DataContext = new BudgetPlannerViewModel();
        }
    }
}
