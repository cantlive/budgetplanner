﻿using CommunityToolkit.Mvvm.Input;
using System.Collections.ObjectModel;
using BudgetPlanner.Model;
using BudgetPlanner.Core;
using CommunityToolkit.Mvvm.ComponentModel;
using System.Windows;
using Microsoft.EntityFrameworkCore;

namespace BudgetPlanner.ViewModel
{
    /// <summary>
    /// ViewModel для окна приложения.
    /// </summary>
    public partial class BudgetPlannerViewModel : ObservableObject
    {
        private readonly SqlLiteDataBaseContext _dataBaseContext = new();

        private ObservableCollection<Transaction> _transactions;
        private double _amountToAdd;
        private string _selectedType;
        private ObservableCollection<string> _types;       

        /// <summary>
        /// Создает экземпляр класса <see cref="BudgetPlannerViewModel"/>
        /// </summary>
        public BudgetPlannerViewModel()
        {
            Types = ["Зачисление", "Снятие"];
            _dataBaseContext.Database.EnsureCreated();
            _dataBaseContext.Transactions.Load();
            Transactions = _dataBaseContext.Transactions.Local.ToObservableCollection();
        }

        /// <summary>
        /// Коллекция транзакций.
        /// </summary>
        public ObservableCollection<Transaction> Transactions
        {
            get => _transactions;
            set => SetProperty(ref _transactions, value);
        }

        /// <summary>
        /// Коллекция типов транзакции.
        /// </summary>
        public ObservableCollection<string> Types
        {
            get => _types;
            set => SetProperty(ref _types, value);
        }

        /// <summary>
        /// Сумма транзакции.
        /// </summary>
        public double AmountToAdd
        {
            get => _amountToAdd;
            set => SetProperty(ref _amountToAdd, value);
        }

        /// <summary>
        /// Выбранный тип транзакции.
        /// </summary>
        public string SelectedType
        {
            get => _selectedType;
            set => SetProperty(ref _selectedType, value);
        }

        /// <summary>
        /// Проверяет, может ли выполняться команда добавления транзакции.
        /// </summary>
        /// <returns><see langword="true"/>, если команда может быть выполнена, в противном случае - <see langword="false"/>.</returns>
        private bool CanAddTransaction()
        {
            return AmountToAdd != 0 && !string.IsNullOrEmpty(SelectedType);
        }

        /// <summary>
        /// Добавляет новую транзакцию в коллекцию и сохраняет в базу данных.
        /// </summary>
        [RelayCommand]
        private void AddTransaction()
        {
            if (!CanAddTransaction())
            {
                MessageBox.Show("Не верно заполненны данные транзакции", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var transaction = new Transaction
            {
                Amount = AmountToAdd,
                Type = SelectedType,
                Date = DateTime.Now
            };
            Transactions.Add(transaction);

            _dataBaseContext.Transactions.Add(transaction);
            _dataBaseContext.SaveChanges();
        }
    }
}
