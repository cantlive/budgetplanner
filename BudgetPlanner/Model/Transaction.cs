﻿namespace BudgetPlanner.Model
{
    /// <summary>
    /// Представляет транзакцию на банковском счете.
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Сумма.
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// Тип (зачисление/снятие).
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Дата.
        /// </summary>
        public DateTime Date { get; set; }
    }
}
